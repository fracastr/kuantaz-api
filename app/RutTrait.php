<?php

namespace App;

trait RutTrait
{
    public static function validarRut($rut) {
        // Eliminar posibles espacios en blanco al inicio y al final del RUT
        $rut = trim($rut);

        // Extraer el dígito verificador
        $dv = substr($rut, -1);

        // Extraer el número del RUT
        $numero = substr($rut, 0, -1);

        // Verificar que el número y el dígito verificador tengan la longitud correcta
        if (strlen($dv) != 1 || strlen($numero) < 1) {
            return false;
        }

        // Calcular el dígito verificador esperado
        $suma = 0;
        $multiplo = 2;
        for ($i = strlen($numero) - 1; $i >= 0; $i--) {
            $suma += $numero[$i] * $multiplo;
            $multiplo = ($multiplo == 7) ? 2 : $multiplo + 1;
        }
        $dvEsperado = 11 - ($suma % 11);
        $dvEsperado = ($dvEsperado == 11) ? 0 : (($dvEsperado == 10) ? 'k' : $dvEsperado);

        // Comparar el dígito verificador esperado con el proporcionado
        return strtolower($dv) === strtolower($dvEsperado);
    }
}
