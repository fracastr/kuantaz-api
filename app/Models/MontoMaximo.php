<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MontoMaximo extends Model
{
    use HasFactory;

    protected $table = 'montos_maximos';
    protected $fillable = ['monto_minimo', 'monto_maximo', 'id_beneficio'];
}
