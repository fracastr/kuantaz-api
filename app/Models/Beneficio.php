<?php

namespace App\Models;

use App\RutTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Beneficio extends Model
{
    use HasFactory, RutTrait;
    
    protected $table = 'beneficios';
    protected $fillable = ['nombre', 'id_ficha'];




    public function monto_maximo(): HasOne
    {
        return $this->hasOne(MontoMaximo::class);
    }

    public function beneficios(): HasMany
    {
        return $this->hasMany(BeneficiosEntregados::class, 'id_beneficio', 'id');
    }

    public function ficha(): HasOne
    {
        return $this->hasOne(Ficha::class, 'id', 'id_ficha');
    }
}
