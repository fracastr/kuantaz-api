<?php

namespace App\Http\Controllers;

use App\Models\Beneficio;
use App\Models\BeneficiosEntregados;
use App\Models\Ficha;
use App\Models\MontoMaximo;
use ArrayObject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use stdClass;

class BeneficioController extends Controller
{

    public function getMisBeneficios($rut){
        //validamos el rut, suponiendo que viene sin puntos ni guion ej: 111111111
        if(Beneficio::validarRut($rut)){

            $rutSinDv = substr($rut, 0, -1);

            $years = BeneficiosEntregados::select(DB::raw('DATE_FORMAT(fecha,"%Y") as year'))
            ->where('run', $rutSinDv)
            ->groupBy('year')
            ->pluck('year');

            $beneficios = array();

            foreach ($years as $keyY => $valueY) {
                $benefits = BeneficiosEntregados::whereRaw('YEAR(fecha) = '.$valueY)->get();

                $benefitsPerYear = new stdClass();
                $benefitsPerYear->year = $valueY;
                $benefitsPerYear->num = 0;
                $benefitsPerYear->montoTotal = '$' . number_format(array_sum(array_column($benefits->toArray(), 'total')), 0, ',', '.');
                $benefitsPerYear->beneficios = array();

                foreach ($benefits as $keyB => $valueB) {
                    $benefit = Beneficio::select(DB::raw('beneficios.id
                    , beneficios.nombre
                    , concat("$", FORMAT(beneficios_entregados.total, 0, "es_CL")) as total
                    , beneficios_entregados.fecha
                    , beneficios_entregados.fecha as mes
                    , beneficios.id_ficha'))
                    ->with(['ficha' => function($query) {
                        $query->select(DB::raw('id, nombre, url, IF(publicada, "true", "false") as publicada'))
                        ->where('publicada', true);
                    }])
                    ->join('beneficios_entregados', 'beneficios_entregados.id_beneficio', 'beneficios.id')
                    ->join('montos_maximos', 'montos_maximos.id_beneficio', 'beneficios.id')
                    ->where('beneficios.id', $valueB->id_beneficio)
                    ->where('montos_maximos.monto_minimo', '<=', $valueB->total)
                    ->where('montos_maximos.monto_maximo', '>=', $valueB->total)
                    ->first();

                    if($benefit != null){
                        $benefit->mes = Carbon::parse($benefit->mes)->monthName;
                        unset($benefit->id_ficha);
                        $benefitsPerYear->num++;
                        array_push($benefitsPerYear->beneficios, $benefit);
                    }
                    else{
                        $monto_total = (int)preg_replace("/[^0-9]/", "", $benefitsPerYear->montoTotal);
                        $benefitsPerYear->montoTotal = '$' . number_format($monto_total - $valueB->total, 0, ',', '.');
                    }
                }
                if(sizeof($benefitsPerYear->beneficios) > 0){
                    array_push($beneficios, $benefitsPerYear);
                }
            }
            $data = new stdClass();
            $data->beneficios = $beneficios;
            return response()->json(
                ['code' => 200,
                'status' => 'success',
                'data' => $data
                ]
            );
        }
        else{
            return ('rut no valido');
        }
    }

    /**
     * Store a newly created ficha in storage.
     */
    public function postFichas(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nombre' => 'required',
            'url' => 'required',
            'publicada' => 'required|boolean'
        ]);
        if ($validator->fails()) {
            // Return errors or redirect back with errors
            return $validator->errors();
        }

        $ficha = Ficha::create($request->only(['nombre', 'url', 'publicada']));
        return response()->json(
            ['code' => 200,
            'status' => 'success',
            'ficha creada' => $ficha
            ]
        );
    }

    /**
     * Store a newly created beneficio in storage.
     */
    public function postBeneficios(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nombre' => 'required',
            'id_ficha' => 'required|exists:ficha,id',
        ]);
        if ($validator->fails()) {
            // Return errors or redirect back with errors
            return $validator->errors();
        }

        $beneficio = Beneficio::create($request->only(['nombre', 'id_ficha']));
        return response()->json(
            ['code' => 200,
            'status' => 'success',
            'beneficio creado' => $beneficio
            ]
        );
    }

    /**
     * Store a newly created usuario in storage.
     */
    public function postUsuarios(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'run' => 'required',
            'dv' => 'required',
            'total' => 'required|numeric',
            'estado' => 'required',
            'fecha' => 'required|date',
            'id_beneficio' => 'required||exists:beneficios,id',
        ]);
        if ($validator->fails()) {
            // Return errors or redirect back with errors
            return $validator->errors();
        }

        $usuario = BeneficiosEntregados::create($request->only(['run', 'dv', 'total', 'estado', 'fecha', 'id_beneficio']));
        return response()->json(
            ['code' => 200,
            'status' => 'success',
            'usuario creado' => $usuario
            ]
        );
    }

    /**
     * Store a newly created montos_maximos in storage.
     */
    public function postMontosMaximos(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'monto_minimo' => 'required|numeric',
            'monto_maximo' => 'required|numeric',
            'id_beneficio' => 'required||exists:beneficios,id',
        ]);
        if ($validator->fails()) {
            // Return errors or redirect back with errors
            return $validator->errors();
        }

        $monto_maximo = MontoMaximo::create($request->only(['monto_minimo', 'monto_maximo', 'id_beneficio']));
        return response()->json(
            ['code' => 200,
            'status' => 'success',
            'monto maximo creado' => $monto_maximo
            ]
        );
    }

}
