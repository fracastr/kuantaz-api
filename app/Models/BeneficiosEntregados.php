<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class BeneficiosEntregados extends Model
{
    use HasFactory;
    
    protected $table = 'beneficios_entregados';
    protected $fillable = ['run', 'dv', 'total', 'estado', 'fecha', 'id_beneficio'];

    public function beneficios(): BelongsTo
    {
        return $this->belongsTo(Beneficio::class, 'id_beneficio', 'id');
    }
}
