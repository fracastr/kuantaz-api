<?php

use App\Http\Controllers\BeneficioController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::get('/misbeneficios/{RUT}', [BeneficioController::class, 'getMisBeneficios']);

Route::post('/usuarios', [BeneficioController::class, 'postUsuarios']);
Route::post('/beneficios', [BeneficioController::class, 'postBeneficios']);
Route::post('/fichas', [BeneficioController::class, 'postFichas']);
Route::post('/montos_maximos', [BeneficioController::class, 'postMontosMaximos']);

