<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('beneficios_entregados', function (Blueprint $table) {
            $table->id();
            $table->string('run');
            $table->string('dv');
            $table->string('total');
            $table->string('estado');
            $table->date('fecha');
            $table->unsignedBigInteger('id_beneficio');

            $table->foreign('id_beneficio')->references('id')->on('beneficios');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('beneficios_entregados');
    }
};
